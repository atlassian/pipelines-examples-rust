#! /bin/bash

set -eux

cargo login ${CARGO_API_KEY}
cargo publish -v || true
# TODO: fix ERROR: crate version `0.1.0` is already uploaded
