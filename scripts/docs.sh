#! /bin/bash

set -eux

cargo doc --release --no-deps
git clone https://${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}@bitbucket.org/${CARGO_LIB_NAME}/${CARGO_LIB_NAME}.bitbucket.org.git target/docwebsite
rm -rvf target/docwebsite/*
cp -rv target/doc/* target/docwebsite/
cp -v docwebsite/index.html target/docwebsite/
cd target/docwebsite
git add -A
git commit -m "bitbucket pipelines, ${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG} commit ${BITBUCKET_COMMIT}" || true
git push
